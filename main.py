import textwrap

def wrap(string, max_width):

    stri_out = ""
    stri_width = ""

    for i in range(len(string)):
      stri_width += string[i]
      if len(stri_width) == max_width:
        stri_out += stri_width + '\n'
        stri_width = ""
      if i == len(string) -1:
        stri_out = stri_out + stri_width  
    return stri_out

if __name__ == '__main__':
    string, max_width = input(), int(input())
    result = wrap(string, max_width)
    print(result)
